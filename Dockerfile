FROM node:16.14.0-alpine

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH
COPY package*.json ./
RUN yarn --frozen-lockfile --non-interactive
COPY . ./
CMD ["yarn", "start"]